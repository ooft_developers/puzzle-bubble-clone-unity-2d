﻿using UnityEngine;
using System.Collections;

public class CollectableManager : MonoBehaviour {
	public CircleCollider2D physicalCollider;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{

		if (col.gameObject.tag == "Ground") {
			CollectCollectable();
		}
	}

	void CollectCollectable( ){
		//for now just blow it up
		Destroy (gameObject);
	}

}
