using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallManager : MonoBehaviour {


	private GameObject collectablePrefab;
	public Dictionary<Joint2D, LineRenderer> joints;
	public float type;
	public float state;
	public bool isLoadedBall = false;
	public AudioClip popSound; 
	public bool isExploding = false;

	BallManager rootBall;
	bool isRootBall = false;

	public List<GameObject> linkedObjects;
	// Use this for initialization
	void Start () {

		LoadSounds();
		this.joints = new Dictionary<Joint2D, LineRenderer> ();
		string filepath = "Collectable";
		collectablePrefab = (GameObject)Instantiate(Resources.Load<GameObject>(filepath));
		collectablePrefab.transform.position = gameObject.transform.position;

		//set the sprite base on the type of this here ball
		string spritePath = "testBall" + type;
		Object loadedObject =  Resources.Load<Sprite>(spritePath);
		Sprite spriteObject = (Sprite)loadedObject;
		SpriteRenderer spriteRenderer = collectablePrefab.GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = spriteObject;
	
		DistanceJoint2D springJoint = gameObject.AddComponent <DistanceJoint2D>() as DistanceJoint2D;
		springJoint.connectedBody = collectablePrefab.GetComponent<Rigidbody2D>();
		springJoint.distance = 0.02f;
	}

	void LoadSounds(){
		this.popSound = Resources.Load("pop") as AudioClip;
	}

	// Update is called once per frame
	void Update () {
		//draw debug lines for all out joints
		foreach (KeyValuePair<Joint2D, LineRenderer> entry in this.joints) {
			LineRenderer line = entry.Value;
			Joint2D joint = entry.Key;
			if(line != null){
				line.SetPosition (0, joint.gameObject.transform.position);
				line.SetPosition (1, joint.connectedBody.gameObject.transform.position);
			}
		}
		//todo, draw slime between connected objects
		//collectablePrefab.transform.position = gameObject.transform.position;
	}

	void OnCollisionEnter2D(Collision2D collision){
		Collider2D col = collision.collider;
		
		if (isLoadedBall) {
			//this is the ball loaded in the cannon we do not want anything to collide with this ball
			return;
		}
		
		if (col.gameObject.tag == "ball") {
			handleBallOnBallCollision(col);
		} else if (col.gameObject.tag == "Terrain") {
			handleBallOnWallCollision(collision);
		}else if (col.gameObject.tag == "Ground") {
			Destroy(gameObject);
		} else if (col.gameObject.tag == "Wall"){
			//TODO: bounce the ball
			//Destroy(gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collision)
	{

	}

	public void blowUpChain(){
		//is this the root ball

		BallManager ballmanager;
		if (!this.isRootBall) {
			ballmanager = this.rootBall;
		} else {
			ballmanager = this;
		}

		if (ballmanager == null) {
			Debug.Log(@"WHAT EVEN THE FUCK!");
		}

		foreach(GameObject obj in rootBall.linkedObjects){
			BallManager bmanager = obj.GetComponent<BallManager>();
			bmanager.isExploding = true;
		}

		this.collectablePrefab.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		StartCoroutine(chainDestroy());
	}

	void handleBallOnWallCollision(Collision2D collision){
		Collider2D col = collision.collider;
		ContactPoint2D contact = collision.contacts[0];
		if (col.gameObject.tag == "Terrain") {
			GameObject ball = contact.otherCollider.gameObject;
			//connect the ball to the roof.
			if(ball != null){
				DistanceJoint2D dJoint = ball.AddComponent <DistanceJoint2D>() as DistanceJoint2D;
				dJoint.connectedAnchor = contact.point;
				//dJoint.anchor = contact.point;
				dJoint.distance = 0.2f;
			}
		}
	}

	void handleBallOnBallCollision(Collider2D col){
		BallManager otherManager = col.gameObject.GetComponent<BallManager>();

		if(otherManager.isExploding || this.isExploding){
			return;
		}
		otherManager.GetComponent<Rigidbody2D>().gravityScale = 1.0f;
		CircleCollider2D collider = col.gameObject.GetComponent<CircleCollider2D>();
		
		if(otherManager.isLoadedBall){
			return;
		}
		
		if(otherManager.type == type){
			//is the other ball already part of a chain
			if(otherManager.rootBall != null){
				//yes it is!
				//are we already part of a chain?
				if(this.rootBall != null){
					//if the ball already part of our chain??
					if(this.rootBall == otherManager.rootBall){
						//do nothing we are already 
					}else{
						Debug.Log("JOIN DA CHAINS!");
						foreach(GameObject ball in this.rootBall.linkedObjects){
							if(!otherManager.rootBall.linkedObjects.Contains(ball)){
								BallManager bm = ball.GetComponent<BallManager>();
								bm.rootBall = otherManager;
								otherManager.rootBall.linkedObjects.Add(ball);
							}
						}
						otherManager.rootBall.linkedObjects.Add(gameObject);
						this.rootBall = otherManager;
						this.linkedObjects.Clear();
					}
					//we must join the chains into one!
				}else{
					//we must add ourselves to this chain!
					if(!otherManager.rootBall.linkedObjects.Contains(col.gameObject)){
						//Debug.Log("ADD SELF TO CHAIN");
						this.rootBall = otherManager.rootBall;
						this.isRootBall = false;
						otherManager.rootBall.linkedObjects.Add(col.gameObject);
					}
				}
			}else{
				//no it is not!
				//are we already part of a chain?
				if(this.rootBall !=null){
					//yes, so we must add this ball to our chain
					if(!this.rootBall.linkedObjects.Contains(col.gameObject)){
						otherManager.rootBall = this.rootBall;
						this.rootBall.linkedObjects.Add(col.gameObject);
					}
				}else{
					//no, we must start a new chain with me at its head
					if(!this.linkedObjects.Contains(col.gameObject)){
						this.rootBall = this;
						this.isRootBall = true;
						otherManager.rootBall = this;
						this.linkedObjects.Add(col.gameObject);
					}
				}
			}
			Debug.Log ("IN CHAIN" + this.rootBall.linkedObjects.Count);			
			if(state == 1){
				BallManager rootBallManager = otherManager.rootBall.GetComponent<BallManager>();
				if(rootBallManager.linkedObjects.Count > 2 && !rootBallManager.isExploding){
					rootBallManager.blowUpChain();
					return;
				}
			}
		}
		if (col.gameObject != null) {
			DistanceJoint2D springJoint = gameObject.AddComponent <DistanceJoint2D> () as DistanceJoint2D;
			springJoint.connectedBody = col.gameObject.GetComponent<Rigidbody2D> ();
			springJoint.distance = 2.1f * collider.radius;
			//GameObject liner = new GameObject();
			//LineRenderer line = liner.AddComponent<LineRenderer>();
			//line.SetVertexCount(2);
			//line.SetWidth(0.1f,0.05f);
			//this.joints.Add(springJoint,line);
			//otherManager.joints.Add(springJoint,line);
		}
	}

	IEnumerator chainDestroy(){
		foreach(KeyValuePair<Joint2D, LineRenderer> entry in this.joints){
			Joint2D joint = entry.Key;
			LineRenderer line = entry.Value;
			line.SetWidth(0,0);
			Destroy(joint);
		}
		this.joints.Clear();
		Debug.Log("chain destroyed" + linkedObjects.Count);
		if (linkedObjects.Count > 0) {
			GameObject ball = linkedObjects[0];
			if(ball==rootBall && linkedObjects.Count > 1){
				ball = linkedObjects[1];
			}
			AudioSource.PlayClipAtPoint (this.popSound,ball.transform.position);
			BallManager manager = ball.GetComponent<BallManager>();
			foreach(KeyValuePair<Joint2D, LineRenderer> entry in manager.joints){
				Joint2D joint = entry.Key;
				LineRenderer line = entry.Value;
				line.SetWidth(0,0);
				Destroy(joint);
			}
			CollectableManager cManager =  manager.collectablePrefab.GetComponent<CollectableManager>();
			Destroy(cManager.physicalCollider);
			linkedObjects.Remove(ball);
			Destroy(ball);
		}

		if (linkedObjects.Count > 0) {
			yield return new WaitForSeconds(0.07f);
			StartCoroutine (chainDestroy ());
		} else {
			AudioSource.PlayClipAtPoint (this.popSound,this.rootBall.transform.position);
			CollectableManager colManager = collectablePrefab.GetComponent<CollectableManager>();
			Destroy(colManager.physicalCollider);
			Destroy (gameObject);
		}
	}
}
