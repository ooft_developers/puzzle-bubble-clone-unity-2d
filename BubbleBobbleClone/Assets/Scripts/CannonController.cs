﻿using UnityEngine;
using System.Collections;



public class CannonController: MonoBehaviour {

	public DistanceJoint2D loadedBallConnector;
	public float rotationSpeed;
	//transform
	Transform myTrans;
	//object position
	Vector3 myPos;
	//object rotation
	Vector3 myRot;
	//object rotation 
	float angle;
	public GameObject cannonShooter;
	public Animator bobblerWinder;
	public GameObject ball;
	public float speed;
	bool ballLoaded;
	GameObject loadedBall;

	// Use this for initialization
	void Start ()
	{
//		speed = 3;
		myTrans = cannonShooter.transform;
		myPos = myTrans.position;
		myRot = myTrans.rotation.eulerAngles;
		bobblerWinder.SetInteger ("Direction",2);
		ballLoaded = false;
		StartCoroutine (LoadBall ());
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		
		//converting the object euler angle's magnitude from to Radians 
		angle = (myTrans.eulerAngles.magnitude+90) * Mathf.Deg2Rad;

		if (Input.GetAxisRaw ("Horizontal") == 0) {
			//Debug.Log ("stopped");
			bobblerWinder.SetInteger ("Direction",2);
			//bobblerWinder.Stop("bobbleWind");
		}
		//rotate object Right & Left
			if (Input.GetAxisRaw("Horizontal") > 0) {
			//Debug.Log ("right");
			bobblerWinder.SetInteger ("Direction",0);
			myRot.z -= rotationSpeed;
			//Debug.Log ("vecor" + cannonShooter.transform.forward.x + " " + cannonShooter.transform.forward.y);
		}
			if (Input.GetAxisRaw("Horizontal") < 0) {
			//Debug.Log ("left");
			bobblerWinder.SetInteger ("Direction",1);
			 myRot.z += rotationSpeed;
			//Debug.Log ("vecor" + cannonShooter.transform.forward.x + " " + cannonShooter.transform.forward.y);
		}

		if (Input.GetButtonDown ("Fire1")) {
			if(ballLoaded){
				FireBall();
			}else{
				//LoadBall();
			}
		}
		//Apply
		myTrans.position = myPos;
		myTrans.rotation = Quaternion.Euler (myRot);

	}

	void FireBall(){
		//give the loaded ball a rigid body
		DestroyObject (loadedBallConnector);
		loadedBall.GetComponent<BallManager> ().isLoadedBall = false;
		loadedBall.transform.parent = null;
		Rigidbody2D ballRigidBody = loadedBall.GetComponent<Rigidbody2D>();
		ballRigidBody.gravityScale = 0.0f;
		ballRigidBody.angularDrag = 999;
		loadedBall.tag = "ball";
		float x = (Mathf.Cos (angle) * 100) * Time.deltaTime;
		float y = (Mathf.Sin (angle) * 100) * Time.deltaTime;

		//Debug.Log ("x" + x + " y" + y );
		loadedBall.GetComponent<Rigidbody2D>().velocity =new Vector3(x*speed,y*speed,0);
		ballLoaded = false;
		//
		StartCoroutine(LoadBall());
	}
	
	IEnumerator LoadBall(){

		yield return new WaitForSeconds(0.5f);
		loadedBall = (GameObject)Instantiate(ball);
		loadedBall.GetComponent<BallManager> ().isLoadedBall = true;
//
		int type = Random.Range(1, 7);
		string filepath = "atestBall1";
		loadedBall.GetComponent<BallManager> ().type = type;
		loadedBall.GetComponent<BallManager> ().state = 1;
		Object loadedObject =  Resources.Load<Sprite>(filepath);
		Sprite spriteObject = (Sprite)loadedObject;
		SpriteRenderer spriteRenderer = loadedBall.GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = spriteObject;

		loadedBallConnector = cannonShooter.AddComponent <DistanceJoint2D>() as DistanceJoint2D;
		loadedBallConnector.connectedBody = loadedBall.GetComponent<Rigidbody2D>();
		loadedBallConnector.distance = 0.01f;

		loadedBall.transform.parent = cannonShooter.transform;
		loadedBall.transform.position = cannonShooter.transform.position;
		ballLoaded = true;
		//newBall
	}
}