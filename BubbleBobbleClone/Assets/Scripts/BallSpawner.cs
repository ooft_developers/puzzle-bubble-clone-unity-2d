﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {
	public GameObject ball;
	bool isSpewing;
	float timer;
	float timer2;

	// Use this for initialization
	void Start () {
		isSpewing = false;
		timer = 9;
		timer2 = 0;
	}
	
	// Update is called once per frame
	void Update()
	{
		timer += Time.deltaTime;
		if(timer >= 100)
		{
			if(!isSpewing){
				SpewBalls();
			}
			timer2 += Time.deltaTime;
			if(timer2 > 1){
				timer= 0;
				timer2= 0;
				StopBalls();
			}
		}
	}
	void StopBalls(){
		isSpewing = false;
		CancelInvoke ();
	}
	void SpewBalls()
	{
		isSpewing = true;
		InvokeRepeating("spawnBall", 0.0f, 0.3f);
	}

	void spawnBall(){
		float xpos = Random.Range(-1,1);

		gameObject.transform.position = new Vector3(xpos, gameObject.transform.position.y, 0);
		GameObject spawnedBall = (GameObject)Instantiate(ball);
		Rigidbody2D body = (Rigidbody2D)spawnedBall.GetComponent<Rigidbody2D>();
		body.gravityScale = 0.1f;
		int type = Random.Range(1, 7);
		string filepath = "atestBall1";
		spawnedBall.GetComponent<BallManager> ().type = type;
		spawnedBall.GetComponent<BallManager> ().state = 0;
		Object loadedObject =  Resources.Load<Sprite>(filepath);
		Sprite spriteObject = (Sprite)loadedObject;
		SpriteRenderer spriteRenderer = spawnedBall.GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = spriteObject;
		
		spawnedBall.transform.position = gameObject.transform.position;
		
		//fire it off at a random angle and velocity.
		
		float x = Random.Range(-3,2);
		float y = Random.Range(1,1);
		
		//Debug.Log ("x" + x + " y" + y );
		spawnedBall.GetComponent<Rigidbody2D>().velocity =new Vector3(x,y*50);
	}


}
